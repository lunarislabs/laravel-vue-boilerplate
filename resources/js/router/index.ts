import { createRouter, createWebHistory } from "vue-router";

import Home from '@/page/Home.vue'
import About from '@/page/About.vue'
import NotFound from '@/layouts/NotFound.vue'
import DefaultLayout from '@/layouts/Default.vue'

const router = createRouter({
  history: createWebHistory(),
  routes: [
    {
      path: '/',
      meta: { layout: DefaultLayout },
      component: Home
    },
    {
      path: '/user',
      meta: { layout: DefaultLayout },
      component: About
    },
    {
      path: '/:pathMatch(.*)*',
      component: NotFound
    }
  ]
})

export default router