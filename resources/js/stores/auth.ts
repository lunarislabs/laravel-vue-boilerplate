import { ref, computed } from 'vue'
import { defineStore } from 'pinia'
import type { Auth } from '@/types/authStore'

export const useAuthStore = defineStore('auth', () => {
  const user = ref<Auth>()
  const isLogin = computed(() => !!user.value?.id ?? false)

  return { user, isLogin }
})