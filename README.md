# Laravel Vue Boilerplate

## TechStack
- Laravel 10
- PHP 8.1
- Vue 3 + Vite
- Typescript
- TailwindCSS 3

## Getting Started
Minimal Setup

1. Install dependency for laravel  
```
composer install
```

2. Install dependency for vue  
```
npm install
```

3. Set the application key  
```
php artisan key:generate
```

4. Run vue application and watch for changes  
```
npm run dev
```

5. Run laravel application and watch for changes  
```
php artisan serve
```